import db from '../../database/models/Test';

const DataRepositorie = {
  getData: async () => {
    const response = await db.test.find();
    console.log('DataRepositorie', response);
    return response;
  },
  setData: async () => {
    const response = await db.test.insertMany({testField: 'test3'});
    console.log('DataRepositorie', response);
    return response;
  },
  updateData: async () => {
    const response = await db.test.findOneAndUpdate(
                                                   [{testField: 'test3'}],
                                                   {$set: {testField: 'test0'}},
                                                   {new: true}
                                                  );
    console.log('DataRepositorie', response);
    return response;
  },
  deleteData: async () => {
    const response = await db.test.deleteMany({testField: 'test0'});
    console.log('DataRepositorie', response);
    return response;
  },
};

export default DataRepositorie;
