import dbUser from '../../database/models/ModelUser';

const UserRepositorie = {
  getData: async () => {
    const response = await dbUser.test.find();
    console.log('UserRepositorie', response);
    return response;
  },
  setData: async () => {
    const response = await dbUser.test.insertMany({user: 
                                                    {userLogin: 'Admin',
                                                    userName: 'Vasiliy',
                                                    userMail: 'root@kola-nn.com',
                                                    userPass: '12345',
                                                    userAge: 42},
                                                });
    console.log('UserRepositorie', response);
    return response;
  },
  updateData: async () => {
    const response = await dbUser.test.findOneAndUpdate({userLogin: 'Admin'},
                                              {$set: {userLogin: 'Administrator'}},
                                              {new: true}
                                              );
    console.log('UserRepositorie', response);
    return response;
  },
  deleteData: async () => {
    const response = await dbUser.test.deleteMany({userName: 'Vasiliy'});
    console.log('UserRepositorie', response);
    return response;
  },
};

export default UserRepositorie;
