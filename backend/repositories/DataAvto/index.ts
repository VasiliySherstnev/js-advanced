import dbAvto from '../../database/models/ModelRusavto';

const AvtoRepositorie = {
  getData: async () => {
    const response = await dbAvto.test.find();
    console.log('AvtoRepositorie', response);
    return response;
  },
  setData: async () => {
    const response = await dbAvto.test.insertMany({
                                                    avtoName: 'MAZ',
                                                    avtoDescription: 'Супер машина',
                                                    avtoPrice: 1000,
                                                    avtoRating: 5,
                                                    avtoType: 'Buisiness',
                                                    },
                                                );
    console.log('AvtoRepositorie', response);
    return response;
  },
  updateData: async () => {
    const response = await dbAvto.test.findOneAndUpdate(
                                              [{avtoName: 'MAZ'}],
                                              {$set: {avtoName: 'KAMAZ'}},
                                              {new: true}
                                              );
    console.log('AvtoRepositorie', response);
    return response;
  },
  deleteData: async () => {
    const response = await dbAvto.test.deleteMany({avtoName: 'MAZ'});
    console.log('AvtoRepositorie', response);
    return response;
  },
};

export default AvtoRepositorie;
