import controllers from '../../controllers';

const data = [
  {
    method: 'GET',
    path: '/',
    handler: (request, h) => {
      return 'Hello World!';
    },

  },

  {
    method: 'GET',
    path: '/data',
    handler: controllers.Data.getData,
  },
  {
    method: 'POST',
    path: '/data',
    handler: controllers.Data.setData,
  },
  {
    method: 'PATCH',
    path: '/data',
    handler: controllers.Data.updateData,
  },
  {
    method: 'DELETE',
    path: '/data',
    handler: controllers.Data.deleteData,
  },

];

export default data;
