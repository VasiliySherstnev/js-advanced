import controllers from '../../controllers';

const dataUser = [
  {
    method: 'GET',
    path: '/user',
    handler: controllers.DataUser.getData,
  },
  {
    method: 'POST',
    path: '/user',
    handler: controllers.DataUser.setData,
  },
  {
    method: 'PATCH',
    path: '/user',
    handler: controllers.DataUser.updateData,
  },
  {
    method: 'DELETE',
    path: '/user',
    handler: controllers.DataUser.deleteData,
  },
];

export default dataUser;
