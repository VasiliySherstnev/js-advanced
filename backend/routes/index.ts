import data from './Data';
import dataAvto from './DataAvto';
import dataUser from './DataUser';

export default [...data, ...dataAvto, ...dataUser];
