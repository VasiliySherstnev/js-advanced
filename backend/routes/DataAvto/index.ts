import controllers from '../../controllers';

const dataAvto = [
  {
    method: 'GET',
    path: '/avto',
    handler: controllers.DataAvto.getData,
  },
  {
    method: 'POST',
    path: '/avto',
    handler: controllers.DataAvto.setData,
  },
  {
    method: 'PATCH',
    path: '/avto',
    handler: controllers.DataAvto.updateData,
  },
  {
    method: 'DELETE',
    path: '/avto',
    handler: controllers.DataAvto.deleteData,
  },
];

export default dataAvto;
