import UserRepositorie from '../../repositories/DataUser';

const DbHandlersUser = {
  getData: (request: any, h: any) => {
    return UserRepositorie.getData();
  },
  setData: (request: any, h: any) => {
    return UserRepositorie.setData();
  },
  updateData: (request: any, h: any) => {
    return UserRepositorie.updateData();
  },
  deleteData: (request: any, h: any) => {
    return UserRepositorie.deleteData();
  },

};

export default DbHandlersUser;
