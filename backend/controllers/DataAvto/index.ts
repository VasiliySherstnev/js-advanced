import AvtoRepositorie from '../../repositories/DataAvto';

const DbHandlersAvto = {
  getData: (request: any, h: any) => {
    return AvtoRepositorie.getData();
  },
  setData: (request: any, h: any) => {
    return AvtoRepositorie.setData();
  },
  updateData: (request: any, h: any) => {
    return AvtoRepositorie.updateData();
  },
  deleteData: (request: any, h: any) => {
    return AvtoRepositorie.deleteData();
  },

};

export default DbHandlersAvto;
