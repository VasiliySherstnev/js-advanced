import DataRepositorie from '../../repositories/Data';

const DbHandlers = {
  getData: (request: any, h: any) => {
    return DataRepositorie.getData();
  },
  setData: (request: any, h: any) => {
    return DataRepositorie.setData();
  },
  updateData: (request: any, h: any) => {
    return DataRepositorie.updateData();
  },
  deleteData: (request: any, h: any) => {
    return DataRepositorie.deleteData();
  },

};

export default DbHandlers;
