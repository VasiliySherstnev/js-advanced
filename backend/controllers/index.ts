import Data from './Data';
import DataAvto from './DataAvto';
import DataUser from './DataUser';

export default { Data, DataAvto, DataUser};
