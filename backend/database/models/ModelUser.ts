import { v4 as uuidv4 } from 'uuid';
import { Document, Schema, Model, connection, model } from 'mongoose';

// const mongoose = require('mongoose');

export const userSchema = new Schema(
  {
    userId: {
      type: String,
      default: uuidv4,
    },
    user: {
        userLogin: String,
        userName: String,
        userMail: String,
        userPass: String,
        userAge: Number,
      }
    },
);

interface Db {
  test: Model<any>;
}

const dbUser: Db = {
  test: model('TestUser', userSchema),
};

export default dbUser;
