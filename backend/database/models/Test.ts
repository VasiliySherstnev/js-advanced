import { v4 as uuidv4 } from 'uuid';
import { Document, Schema, Model, connection, model } from 'mongoose';

// const mongoose = require('mongoose');

export const testSchema = new Schema(
  {
    testId: {
      type: String,
      default: uuidv4,
    },

    testField: {
      type: String,
      required: true,
      unique: true,
    },
  },
  {
    timestamps: true,
  }
);

interface Db {
  test: Model<any>;
}

const db: Db = {
  test: model('Test', testSchema),
};

export default db;
