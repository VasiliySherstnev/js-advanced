import { v4 as uuidv4 } from 'uuid';
import { Document, Schema, Model, connection, model } from 'mongoose';

// const mongoose = require('mongoose');

export const avtoSchema = new Schema(
  {
    avtoId: {
      type: String,
      default: uuidv4,
    },

    avtoName: {
      type: String,
    },

    avtoDescription: {
        type: String,
    },

    avtoPrice: {
      type: Number,
    },

    avtoRating: {
      type: Number,
    },
    
    avtoType: {
      type: String,
    },

    avtoImage: {
        type: String,
      },

  },
);

interface Db {
  test: Model<any>;
}

const dbAvto: Db = {
  test: model('TestAvto', avtoSchema),
};

export default dbAvto;
