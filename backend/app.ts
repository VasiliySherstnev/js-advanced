import * as Hapi from '@hapi/hapi';
import { connect } from 'mongoose';
import routes from './routes';
// const mongoose = require('mongoose');

class App {
  private async initServer() {
    const server = Hapi.server({
      port: 3000,
      host: 'localhost',
    });
    server.route(routes);
    await server.start();

    server.events.on('response', request => {
      const {
        info: { remoteAddress },
      } = request;
      const response = <Hapi.ResponseObject>request.response;
      const isSwagger = request.path.includes('/swagger');
      if (isSwagger) return true;
      const isDocumentation = request.path.includes('/documentation');
      return true;
    });
    console.log('Сервер запущен на %s', server.info.uri);
    process.on('unhandledRejection', err => {
      console.log(err);
      process.exit(1);
    });
  }

  private async initDb() {
    connect('mongodb://localhost:27017/test', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
  }

  public start() {
    this.initServer();
    this.initDb();
  }
}

export default new App();
